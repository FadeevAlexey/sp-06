package ru.fadeev.tm.repository;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.config.AppConfig;

@RunWith(SpringRunner.class)
@SpringJUnitConfig(AppConfig.class)
public abstract class AbstractRepository {

    @Autowired
    protected IUserRepository userRepository;
}
