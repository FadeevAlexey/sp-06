package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.specification.Specifications;

import java.util.UUID;

import static org.junit.Assert.*;

public class TaskRepositoryTest extends AbstractRepository {

    @Autowired
    ITaskRepository taskRepository;

    @Test
    public void saveTaskTest() {
        @Nullable Task task = new Task();
        task.setName("testUser");
        taskRepository.save(task);
        assertNotNull(taskRepository.findById(task.getId()));
        taskRepository.deleteById(task.getId());
    }

    @Test
    public void deleteByIdTaskTest() {
        @Nullable Task task = new Task();
        task.setName("testUser");
        taskRepository.save(task);
        taskRepository.deleteById(task.getId());
        task = taskRepository.findById(task.getId()).orElse(null);
        assertNull(task);
    }

    @Test
    @Transactional
    public void deleteUserTask() {
        @Nullable User user = userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null);
        @Nullable User admin = userRepository.findOne(Specifications.findUserByLogin("Admin")).orElse(null);
        @Nullable Task task = new Task();
        task.setName("testTask");
        task.setUser(user);
        taskRepository.saveAndFlush(task);
        taskRepository.removeByUser_IdAndId(admin.getId(), task.getId());
        assertEquals("testTask", task.getName());
        task = taskRepository.getOne(task.getId());
        taskRepository.removeByUser_IdAndId(user.getId(), task.getId());
        assertFalse(taskRepository.findById(task.getId()).isPresent());
    }

    @Test
    @Transactional
    public void getOneTaskTestUserId() {
        @Nullable User user = userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null);
        @Nullable User admin = userRepository.findOne(Specifications.findUserByLogin("Admin")).orElse(null);
        @Nullable Task task = new Task();
        task.setName("testTask");
        task.setUser(user);
        taskRepository.saveAndFlush(task);
        assertFalse(taskRepository.findOne(Specifications.findOne(admin.getId(), task.getId())).isPresent());
        task = taskRepository.findOne(Specifications.findOne(user.getId(), task.getId())).orElse(null);
        assertEquals("testTask", task.getName());
        taskRepository.removeByUser_IdAndId(user.getId(), task.getId());
    }

    @Test
    public void findOneTaskTest() {
        @Nullable Task task = new Task();
        task.setName("testTask");
        taskRepository.saveAndFlush(task);
        task = taskRepository.findById(task.getId()).orElse(null);
        assertEquals("testTask", task.getName());
        taskRepository.deleteById(task.getId());
        assertFalse(taskRepository.findById(task.getId()).isPresent());
        assertFalse(taskRepository.findById(UUID.randomUUID().toString()).isPresent());
    }

    @Test
    public void findAllTest() {
        final int tasksCount = taskRepository.findAll().size();
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        taskRepository.save(task);
        taskRepository.save(task2);
        taskRepository.save(task3);
        final int finalCount = taskRepository.findAll().size();
        Assert.assertEquals(tasksCount, finalCount - 3);
        taskRepository.deleteById(task.getId());
        taskRepository.deleteById(task2.getId());
        taskRepository.deleteById(task3.getId());
    }

    @Test
    @Transactional
    public void findAllByUserIdTest() {
        @Nullable User user = userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null);
        @Nullable User admin = userRepository.findOne(Specifications.findUserByLogin("Admin")).orElse(null);
        final int tasksCount = taskRepository.findAll(Specifications.findAllByUserId(user.getId())).size();
        @NotNull Task task = new Task();
        task.setUser(user);
        @NotNull Task task2 = new Task();
        task2.setUser(user);
        @NotNull Task task3 = new Task();
        task3.setUser(user);
        taskRepository.save(task);
        taskRepository.save(task2);
        taskRepository.save(task3);
        final int finalCount = taskRepository.findAll(Specifications.findAllByUserId(user.getId())).size();
        Assert.assertEquals(tasksCount, finalCount - 3);
        @Nullable Task taskAdminTest = taskRepository.findOne(Specifications.findOne(admin.getId(), task.getId())).orElse(null);
        assertNull(taskAdminTest);
        taskRepository.deleteById(task.getId());
        taskRepository.deleteById(task2.getId());
        taskRepository.deleteById(task3.getId());
    }

    @Test
    public void mergeTaskTest() {
        Task task = new Task();
        task.setName("Igor");
        taskRepository.save(task);
        assertEquals("Igor", taskRepository.findById(task.getId()).orElse(null).getName());
        task.setName("Vitalik");
        taskRepository.save(task);
        assertEquals("Vitalik", taskRepository.findById(task.getId()).orElse(null).getName());
        taskRepository.deleteById(task.getId());
    }

    @Test
    @Transactional
    public void removeAllTaskTest() {
        @Nullable User user = userRepository.findOne(Specifications.findUserByLogin("User")).orElse(null);
        @NotNull Task task = new Task();
        task.setUser(user);
        taskRepository.save(task);
        @NotNull Task task2 = new Task();
        task2.setUser(user);
        taskRepository.save(task2);
        assertTrue(taskRepository.findAll(Specifications.findAllByUserId(user.getId())).size() > 1);
        taskRepository.removeAllByUser_Id(user.getId());
        assertTrue(taskRepository.findAll(Specifications.findAllByUserId(user.getId())).isEmpty());
    }

}
