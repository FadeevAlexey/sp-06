package ru.fadeev.tm.marshalling;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.config.AppConfig;
import ru.fadeev.tm.config.WebConfig;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.enumerated.Status;

import java.util.Date;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringJUnitConfig(AppConfig.class)
@ContextConfiguration(classes = {WebConfig.class})
public class MarshallingTest  {

    @Autowired
    ITaskService taskService;

    @Autowired
    private MappingJackson2HttpMessageConverter springMvcJacksonConverter;

    @Test
    public void marshallingTaskTest() throws Exception {
        @NotNull final ObjectMapper objectMapper = springMvcJacksonConverter.getObjectMapper();
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("testName");
        taskDTO.setDescription("testDescription");
        taskDTO.setUserId("testUserId");
        taskDTO.setProjectId("testProjectId");
        taskDTO.setStatus(Status.DONE);
        taskDTO.setProjectName("testProjectName");
        taskDTO.setStartDate(new Date(System.currentTimeMillis()));
        taskDTO.setFinishDate(new Date(System.currentTimeMillis()));
        @NotNull final String json = (objectMapper.writeValueAsString(taskDTO));
        @NotNull final TaskDTO result = objectMapper.readValue(json, TaskDTO.class);
        assertEquals(taskDTO.getId(), result.getId());
        assertEquals(taskDTO.getDescription(), result.getDescription());
        assertEquals(taskDTO.getStartDate(), result.getStartDate());
        assertEquals(taskDTO.getFinishDate(), result.getFinishDate());
        assertEquals(taskDTO.getStatus(), result.getStatus());
        assertEquals(taskDTO.getName(), result.getName());
        assertEquals(taskDTO.getProjectName(), result.getProjectName());
        assertEquals(taskDTO.getCreationTime(), result.getCreationTime());
        assertEquals(taskDTO.getUserId(), result.getUserId());
    }

    @Test
    public void marshallingProjectTest() throws Exception {
        @NotNull final ObjectMapper objectMapper = springMvcJacksonConverter.getObjectMapper();
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("testName");
        projectDTO.setDescription("testDescription");
        projectDTO.setUserId("testUserId");
        projectDTO.setStatus(Status.DONE);
        projectDTO.setStartDate(new Date(System.currentTimeMillis()));
        projectDTO.setFinishDate(new Date(System.currentTimeMillis()));
        @NotNull final String json = (objectMapper.writeValueAsString(projectDTO));
        @NotNull final ProjectDTO result = objectMapper.readValue(json, ProjectDTO.class);
        assertEquals(projectDTO.getId(), result.getId());
        assertEquals(projectDTO.getDescription(), result.getDescription());
        assertEquals(projectDTO.getStartDate(), result.getStartDate());
        assertEquals(projectDTO.getFinishDate(), result.getFinishDate());
        assertEquals(projectDTO.getStatus(), result.getStatus());
        assertEquals(projectDTO.getName(), result.getName());
        assertEquals(projectDTO.getCreationTime(), result.getCreationTime());
        assertEquals(projectDTO.getUserId(), result.getUserId());
    }

    @Test
    public void marshallingUserTest() throws Exception {
        @NotNull final ObjectMapper objectMapper = springMvcJacksonConverter.getObjectMapper();
        UserDTO userDTO = new UserDTO();
        userDTO.setLogin("testName");
        userDTO.setEmail("testEmail");
        userDTO.setPassword("testPassword");
        @NotNull final String json = (objectMapper.writeValueAsString(userDTO));
        @NotNull final UserDTO result = objectMapper.readValue(json, UserDTO.class);
        assertEquals(userDTO.getId(), userDTO.getId());
        assertEquals(userDTO.getLogin(), result.getLogin());
        assertEquals(userDTO.getPassword(), result.getPassword());
        assertEquals(userDTO.getEmail(), result.getEmail());
    }

}
