package ru.fadeev.tm.restcontroller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;
import ru.fadeev.tm.client.ProjectClient;
import ru.fadeev.tm.client.TaskClient;
import ru.fadeev.tm.client.UserClient;
import ru.fadeev.tm.config.AppConfig;

@Getter
@Setter
@RunWith(SpringRunner.class)
@SpringJUnitConfig(AppConfig.class)
public abstract class AbstractRestApiTest {

    @NotNull
    @Autowired
    protected ProjectClient projectClient;

    @Autowired
    @NotNull
    protected TaskClient taskClient;

    @Autowired
    @NotNull
    protected UserClient userClient;

    @NotNull
    final protected String userProfile = auth("User", "user");

    @NotNull
    final protected String adminProfile = auth("Admin", "admin");

    protected String auth(@NotNull final String username, final String password) {
        byte[] encodedBytes = Base64Utils.encode((username + ":" + password).getBytes());
        return "Basic " + new String(encodedBytes);
    }

}
