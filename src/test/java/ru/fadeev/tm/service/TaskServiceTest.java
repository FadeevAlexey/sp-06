package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.entity.User;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;

public class TaskServiceTest extends AbstractServiceTest {

    @Autowired
    ITaskService taskService;

    @Autowired
    IProjectService projectService;

    @Test
    public void persistTaskTest() {
        @NotNull Task task = new Task();
        task.setName("testTask");
        taskService.persist(task);
        assertNotNull(taskService.findOne(task.getId()));
        taskService.remove(task.getId());
    }

    @Test
    public void removeTaskTest() {
        @Nullable Task task = new Task();
        task.setName("testUser");
        taskService.persist(task);
        taskService.remove(task.getId());
        task = taskService.findOne(task.getId());
        assertNull(task);
    }

    @Test
    public void removeTaskTestUserId() {
        @Nullable User user = userService.findUserByLogin("User");
        @Nullable User admin = userService.findUserByLogin("Admin");
        @Nullable Task task = new Task();
        task.setName("testTask");
        task.setUser(user);
        taskService.persist(task);
        taskService.remove(admin.getId(), task.getId());
        task = taskService.findOne(task.getId());
        assertEquals("testTask", task.getName());
        taskService.remove(user.getId(), task.getId());
        assertNull(taskService.findOne(task.getId()));
    }

    @Test
    public void findOneTaskTestUserId() {
        @Nullable User user = userService.findUserByLogin("User");
        @Nullable User admin = userService.findUserByLogin("Admin");
        @Nullable Task task = new Task();
        task.setName("testTask");
        task.setUser(user);
        taskService.persist(task);
        assertNull(taskService.findOne(admin.getId(), task.getId()));
        task = taskService.findOne(user.getId(), task.getId());
        assertEquals("testTask", task.getName());
        taskService.remove(task.getId());
    }

    @Test
    public void findOneTaskTest() {
        @Nullable Task task = new Task();
        task.setName("testTask");
        taskService.persist(task);
        task = taskService.findOne(task.getId());
        assertEquals("testTask", task.getName());
        taskService.remove(task.getId());
        assertNull(taskService.findOne(null));
        assertNull(taskService.findOne(UUID.randomUUID().toString()));
    }

    @Test
    public void findAllTest() {
        final int tasksCount = taskService.findAll().size();
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        taskService.persist(task);
        taskService.persist(task2);
        taskService.persist(task3);
        final int finalCount = taskService.findAll().size();
        Assert.assertEquals(tasksCount, finalCount - 3);
        taskService.remove(task.getId());
        taskService.remove(task2.getId());
        taskService.remove(task3.getId());
    }

    @Test
    public void findAllUserId() {
        @Nullable User user = userService.findUserByLogin("User");
        @Nullable User admin = userService.findUserByLogin("Admin");
        final int tasksCount = taskService.findAll(user.getId()).size();
        @NotNull Task task = new Task();
        task.setUser(user);
        @NotNull Task task2 = new Task();
        task2.setUser(user);
        @NotNull Task task3 = new Task();
        task3.setUser(user);
        taskService.persist(task);
        taskService.persist(task2);
        taskService.persist(task3);
        final int finalCount = taskService.findAll(user.getId()).size();
        Assert.assertEquals(tasksCount, finalCount - 3);
        @Nullable Task taskAdminTest = taskService.findOne(admin.getId(), task.getId());
        assertNull(taskAdminTest);
        taskService.remove(task.getId());
        taskService.remove(task2.getId());
        taskService.remove(task3.getId());
    }

    @Test
    public void convertToTaskTest() {
        @Nullable User user = userService.findUserByLogin("User");
        Project project = new Project();
        project.setUser(user);
        project.setName("name");
        projectService.persist(project);
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(user.getId());
        taskDTO.setProjectName("test");
        taskDTO.setProjectName("name");
        taskDTO.setFinishDate(new Date(System.currentTimeMillis()));
        taskDTO.setStartDate(new Date(System.currentTimeMillis()));
        taskDTO.setDescription("testDescription");
        Task task = taskService.convertToTask(taskDTO);
        assertEquals(taskDTO.getId(), task.getId());
        assertEquals(taskDTO.getName(), task.getName());
        assertEquals(taskDTO.getDescription(), task.getDescription());
        assertEquals(taskDTO.getFinishDate(), task.getFinishDate());
        assertEquals(taskDTO.getUserId(), task.getUser().getId());
        assertEquals(taskDTO.getProjectName(), task.getProject().getName());
        assertEquals(taskDTO.getStatus(), task.getStatus());
        projectService.remove(project.getId());
    }

    @Test
    public void mergeTaskTest() {
        Task task = new Task();
        task.setName("Igor");
        taskService.persist(task);
        assertEquals("Igor", taskService.findOne(task.getId()).getName());
        task.setName("Vitalik");
        taskService.merge(task);
        assertEquals("Vitalik", taskService.findOne(task.getId()).getName());
        taskService.remove(task.getId());
    }

    @Test
    public void removeAllTaskTest() {
        @Nullable User user = userService.findUserByLogin("User");
        @NotNull Task task = new Task();
        task.setUser(user);
        taskService.persist(task);
        @NotNull Task task2 = new Task();
        task2.setUser(user);
        taskService.persist(task2);
        assertTrue(taskService.findAll(user.getId()).size() > 1);
        taskService.removeAll(user.getId());
        assertTrue(taskService.findAll(user.getId()).isEmpty());
    }

}