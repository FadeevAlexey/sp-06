package ru.fadeev.tm.controller;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.config.AppConfig;
import ru.fadeev.tm.config.WebConfig;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringJUnitConfig(AppConfig.class)
@ContextConfiguration(classes = {WebConfig.class})
public abstract class AbstractController {

    @Autowired
    protected IUserService userService;

    @Autowired
    private WebApplicationContext context;

    protected MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.
                webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

}
