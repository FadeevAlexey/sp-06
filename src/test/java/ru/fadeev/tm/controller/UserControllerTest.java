package ru.fadeev.tm.controller;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.util.TestUtil;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserControllerTest extends AbstractController {

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void userViewTest() throws Exception {
        mvc.perform(get("/user/view"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("user_view"))
                .andExpect(model().attribute("login", "Admin"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void userEditTest() throws Exception {
        mvc.perform(get("/user/edit"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("user_edit"))
                .andExpect(model().attribute("login", "Admin"))
                .andReturn().getModelAndView();
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void userEditPost() throws Exception {
        User user = userService.findUserByLogin("Admin");
        final String testEmail = UUID.randomUUID().toString();
        ModelAndView modelAndView = mvc.perform(post("/user/edit/").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "email", testEmail)))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andReturn().getModelAndView();

        final UserDTO userDTO = (UserDTO) modelAndView.getModel().get("userDTO");
        Assert.assertEquals(user.getId(), userDTO.getId());
        Assert.assertEquals(testEmail, userService.findUserByLogin("Admin").getEmail());
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void userAdminUsersTest() throws Exception {
        mvc.perform(get("/admin/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("user_list"))
                .andExpect(model().attributeExists("users", "login"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void userAdminViewTest() throws Exception {
        final User user = userService.findUserByLogin("User");
        mvc.perform(get("/admin/view/" + user.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("user_view"))
                .andExpect(model().attribute("login", "User"))
                .andExpect(model().attributeExists("projectsCount", "tasksCount"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void userAdminRemoveTest() throws Exception {
        final User user = new User();
        user.setLogin(UUID.randomUUID().toString());
        userService.persist(user);
        mvc.perform(get("/admin/remove/" + user.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/admin/users"));
        Assert.assertFalse(userService.isLoginExist(user.getLogin()));
    }

    @Test
    public void userCreateTestPost() throws Exception {
       final String login = UUID.randomUUID().toString();
       final ModelAndView modelAndView = mvc.perform(post("/registration").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity("login", login)))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andReturn().getModelAndView();
        final UserDTO userDTO = (UserDTO) modelAndView.getModel().get("userDTO");
        Assert.assertNotNull(userService.findUserByLogin(login));
        userService.remove(userDTO.getId());
    }

}