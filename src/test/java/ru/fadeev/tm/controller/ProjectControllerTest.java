package ru.fadeev.tm.controller;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.util.TestUtil;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProjectControllerTest extends AbstractController {

    @Autowired
    private IProjectService projectService;

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void projectsViewTest() throws Exception {
        mvc.perform(get("/project"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project_list"))
                .andExpect(model().attributeExists("login", "projects"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void projectCreateTestGet() throws Exception {
        mvc.perform(get("/project/create"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project_create"));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void projectCreateTestPost() throws Exception {
        ModelAndView modelAndView = mvc.perform(post("/project/create").contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testName",
                        "description", "testDescription"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andReturn().getModelAndView();

        final ProjectDTO projectDTO = (ProjectDTO) modelAndView.getModel().get("projectDTO");
        Assert.assertEquals("testName", projectDTO.getName());
        Assert.assertEquals("testDescription", projectDTO.getDescription());
        Assert.assertEquals(userService.findUserByLogin("Admin").getId(), projectDTO.getUserId());
        projectService.remove(projectDTO.getId());
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void projectRemoveTest() throws Exception {
        final Project project = new Project();
        project.setUser(userService.findUserByLogin("Admin"));
        project.setName(UUID.randomUUID().toString());
        projectService.persist(project);
        mvc.perform(get("/project/remove/" + project.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/project"));
        Assert.assertNull(projectService.findOne(project.getId()));
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void projectViewTest() throws Exception {
        final Project project = new Project();
        project.setUser(userService.findUserByLogin("Admin"));
        project.setName(UUID.randomUUID().toString());
        projectService.persist(project);
        ModelAndView modelAndView = mvc.perform(get("/project/view/" + project.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getModelAndView();
        ProjectDTO projectDTO = (ProjectDTO) modelAndView.getModel().get("project");
        Assert.assertEquals(project.getId(), projectDTO.getId());
        projectService.remove(projectDTO.getId());
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void projectEditGet() throws Exception {
        final Project project = new Project();
        project.setUser(userService.findUserByLogin("Admin"));
        project.setName(UUID.randomUUID().toString());
        projectService.persist(project);
        final ModelAndView modelAndView = mvc.perform(get("/project/edit/" + project.getId()))
                .andDo(print())
                .andExpect(view().name("project_edit"))
                .andExpect(status().isOk())
                .andReturn().getModelAndView();

        ProjectDTO projectDTO = (ProjectDTO) modelAndView.getModel().get("project");
        Assert.assertEquals(project.getId(), projectDTO.getId());
        projectService.remove(project.getId());
    }

    @Test
    @WithUserDetails(value = "Admin", userDetailsServiceBeanName = "userDetailsService")
    public void projectEditPost() throws Exception {
        final Project project = new Project();
        project.setUser(userService.findUserByLogin("Admin"));
        project.setName(UUID.randomUUID().toString());
        projectService.persist(project);
        ModelAndView modelAndView = mvc.perform(post("/project/edit/" + project.getId()).contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(TestUtil.buildUrlEncodedFormEntity(
                        "name", "testNameChange",
                        "description", "testDescriptionChange"
                )))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andReturn().getModelAndView();

        final ProjectDTO projectDTO = (ProjectDTO) modelAndView.getModel().get("projectDTO");
        Assert.assertEquals(project.getId(), projectDTO.getId());
        Assert.assertEquals("testNameChange", projectDTO.getName());
        Assert.assertEquals("testDescriptionChange", projectDTO.getDescription());
        Assert.assertEquals(userService.findUserByLogin("Admin").getId(), projectDTO.getUserId());
        projectService.remove(project.getId());
    }

}