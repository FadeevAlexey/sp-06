package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.SessionUser;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/project")
    public ModelAndView projectListGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @NotNull final List<ProjectDTO> projects = projectService.findAll(userId)
                .stream()
                .map(ProjectDTO::new)
                .collect(Collectors.toList());
        @NotNull final ModelAndView model = new ModelAndView("project_list");
        model.addObject("login", sessionUser.getUsername());
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/project/create")
    public ModelAndView projectCreateGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final ModelAndView model = new ModelAndView("project_create");
        model.addObject("login", sessionUser.getUsername());
        model.addObject("login", sessionUser.getUsername());
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/project/create")
    public ModelAndView projectCreatePost(
            @NotNull final Authentication authentication,
            @NotNull final ProjectDTO projectDTO
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        projectDTO.setUserId(userId);
        @NotNull final Project project = projectService.convertToProject(projectDTO);
        System.out.println(project);
        System.out.println(projectDTO);
        projectService.persist(project);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project");
        modelAndView.addObject("login", sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/project/remove/{id}")
    public ModelAndView projectRemoveGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        projectService.remove(userId, id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project");
        modelAndView.addObject("login", sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/project/view/{id}")
    public ModelAndView projectViewGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @Nullable final Project project = projectService.findOne(userId, id);
        @NotNull final ModelAndView model = new ModelAndView("project_view");
        model.addObject("login", sessionUser.getUsername());
        if (project == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        model.addObject("project", new ProjectDTO(project));
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/project/edit/{id}")
    public ModelAndView projectEditGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final ModelAndView model = new ModelAndView("project_edit");
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @Nullable final Project project = projectService.findOne(userId, id);
        if (project == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        @Nullable final ProjectDTO projectDTO = new ProjectDTO(project);
        model.addObject("login", sessionUser.getUsername());
        model.addObject("project", projectDTO);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/project/edit/{id}")
    public ModelAndView projectEditPost(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull String id,
            @NotNull final ProjectDTO projectDTO
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        projectDTO.setId(id);
        projectDTO.setUserId(userId);
        @Nullable final Project project = projectService.convertToProject(projectDTO);
        @NotNull ModelAndView model = new ModelAndView("redirect:/project");
        model.addObject("login", sessionUser.getUsername());
        projectService.merge(project);
        model.setStatus(HttpStatus.OK);
        return model;
    }

}