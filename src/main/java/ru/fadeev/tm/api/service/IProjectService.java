package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @Nullable
    Project findOne(@Nullable String userId, @Nullable String id);

    void remove(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Project convertToProject(@NotNull final ProjectDTO projectDTO);

    void removeAll();

    void removeAll(@Nullable String userId);

}