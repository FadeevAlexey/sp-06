package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.entity.Role;
import ru.fadeev.tm.entity.SessionUser;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.specification.Specifications;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final @NotNull User user = findByLogin(username);
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPasswordHash());
        final List<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (Role role : userRoles) roles.add(role.getRole().toString());
        builder.roles(roles.toArray(new String[]{}));
        UserDetails details = builder.build();

        org.springframework.security.core.userdetails.User result = null;
        result = (org.springframework.security.core.userdetails.User) details;

        final SessionUser sessionUser = new SessionUser(result);
        sessionUser.setUserId(user.getId());
        return sessionUser;
    }

    private User findByLogin(@NotNull final String username) {
        return userRepository.findOne(
                Specifications.findUserByLogin(username))
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

}
