package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.specification.Specifications;

import java.util.*;

@Service
@Transactional
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;


    @NotNull
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findOne(Specifications.findOne(userId, id)).orElse(null);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.deleteById(id);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.removeByUser_IdAndId(userId, projectId);
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.save(project);
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.save(project);
    }

    @Override
    public void removeAll() {
        projectRepository.deleteAllInBatch();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        return projectRepository.findAll(Specifications.findAllByUserId(userId));
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUser_Id(userId);
    }


    @NotNull
    public Project convertToProject(@NotNull final ProjectDTO projectDTO) {
        @Nullable Project project = findOne(projectDTO.getUserId(), projectDTO.getId());
        if (project == null) project = new Project();
        project.setId(projectDTO.getId());
        if (projectDTO.getUserId() != null) project.setUser(userRepository.getOne(projectDTO.getUserId()));
        if (!projectDTO.getName().isEmpty()) project.setName(projectDTO.getName());
        if (projectDTO.getDescription() != null && !projectDTO.getDescription().isEmpty())
            project.setDescription(projectDTO.getDescription());
        project.setStartDate(projectDTO.getStartDate());
        project.setFinishDate(projectDTO.getFinishDate());
        project.setStatus(projectDTO.getStatus());
        project.setCreationTime(projectDTO.getCreationTime());
        return project;
    }

}